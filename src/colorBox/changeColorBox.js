import { Component, createRef } from "react";

export class ChangeColorBox extends Component {
    constructor(props) {
        super(props);
        this.myRef = createRef();
    }

    state = {
        value: 'white'
    }
    startingValues = {
        width: 100,
        height: 100
    }

    changeSize = () => {
        this.startingValues.width += 10;
        this.startingValues.height += 10;
        this.myRef.current.style.width = this.startingValues.width + 'px';
        this.myRef.current.style.height = this.startingValues.height + 'px';
    }
    changeColor = (event) => {
        this.myRef.current.style.background = event.target.value;
        this.setState({value: event.target.value});
        console.log(event.target.value);

    };

    render() {
        return(
            <>
            <div style={{width:100, height:100, border: '1px solid black' }}ref={this.myRef}>Random ref box</div>
            <button onClick={this.changeSize}>Change size</button>
           {/* <button onClick={this.changeColor}>Change color</button> */}
            <select id="lang" onChange={this.changeColor} value={this.state.value}>
                  <option value="white">Default</option>
                  <option value="red">Red</option>
                  <option value="Yellow">Yellow</option>
                  <option value="Grey">Grey</option>
               </select>
            </>
        )
    }
}