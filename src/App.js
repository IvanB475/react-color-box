import logo from './logo.svg';
import './App.css';
import { ChangeColorBox } from './colorBox/changeColorBox';

function App() {
  return (
    <div className="App">
      <ChangeColorBox></ChangeColorBox>
    </div>
  );
}

export default App;
